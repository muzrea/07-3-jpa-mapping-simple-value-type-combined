package com.twuc.webApp.domain;

import com.twuc.webApp.domain.composite.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase{
    @Autowired
    private CompanyProfileRepository companyProfileRepository;
    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_save_and_get_entity() {
        final ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(em ->{
            CompanyProfile companyProfile1 = companyProfileRepository.save(new CompanyProfile(new Address("Wuhan", "Gao xin")));
            expectedId.setValue(companyProfile1.getId());
        });

        CompanyProfile companyProfile2 = companyProfileRepository.findById(expectedId.getValue()).get();
        companyProfileRepository.flush();
        assertEquals("Wuhan",companyProfile2.getAddress().getCity());
        assertEquals("Gao xin",companyProfile2.getAddress().getStreet());
    }

    @Test
    void should_save_and_get_userProfile_entity() {
        final ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(em ->{
            UserProfile userProfile = userProfileRepository.save(new UserProfile(new Address("Wuhan", "Gao xin")));
            expectedId.setValue(userProfile.getId());
        });

        UserProfile userProfile1 = userProfileRepository.findById(expectedId.getValue()).get();
        userProfileRepository.flush();
        assertEquals("Wuhan",userProfile1.getAddress().getCity());
        assertEquals("Gao xin",userProfile1.getAddress().getStreet());
    }
}
